import EventEmitter from "eventemitter3";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.emojis = [];
    this.banana = "🍌";
    this.emit(Application.events.READY);
  }

  setEmojis(emojis) {
    this.emojis = emojis;
    document.querySelector("#emojis").innerHTML = "";
    for (const emoji of this.emojis) {
      const paragraph = document.createElement("p");
      paragraph.textContent = emoji;
      document.querySelector("#emojis").appendChild(paragraph);
    }
  }

  addBananas() {
    this.setEmojis(this.emojis.map((monkey) => monkey + this.banana));
  }
}
